import logo from './logo.svg';
import './App.css';
import img1 from './assets/img1.jpeg';
import img2 from '../src/assets/img2.jpeg';
import img3 from '../src/assets/img3.jpeg';
import img4 from '../src/assets/img4.jpeg';
import img5 from '../src/assets/img5.jpeg';
function App() {
	return (
		<div className='App'>
			<header className='App-header'>
				<img src={logo} className='App-logo' alt='logo' />
				<p>
					Edit <code>src/App.js</code> and save to reload.
				</p>
				<a
					className='App-link'
					href='https://reactjs.org'
					target='_blank'
					rel='noopener noreferrer'
				>
					Learn React
				</a>
			</header>
			<div>
				<img src={img1} alt='imagen1' />
				<img src={img2} alt='imagen2' />
				<img src={img3} alt='imagen3' />
				<img src={img4} alt='imagen4' />
				<img src={img5} alt='imagen5' />
			</div>
		</div>
	);
}

export default App;
